package com.health.entity;
import java.util.ArrayList;
public class HealthMainClass {
	
	public static void main(String[] args) {
		
		System.out.println("Class is started");
		User user = new User();
		user.setName("Norman Gomes");
		user.setAge(34);
		user.setGender("Male");
		
		ArrayList<String> habbits = new ArrayList<String>();
		habbits.add("Daily exercise");
		user.setHabits(habbits);
		HealthMainClass hm = new HealthMainClass();
	//	hm.createInsurancePremium(user);
		System.out.println("Class is ended");
	} 
	public double createInsurancePremium(User user){
		
		double premium = getPremium(user);
		return premium;
		
	}
	public double getPremium(User user) {
		
		    //base premium
		    double premium = 5000;

		    // Increase premium depending on age
		    if (user.getAge() >= 18) {
		        premium *= 1.1;
		    }
		    if (user.getAge() >= 25) {
		        premium *= 1.1;
		    }
		    if (user.getAge() >= 30) {
		        premium *= 1.1;
		    }
		    if (user.getAge() >= 35) {
		        premium *= 1.1;
		    }
		    if (user.getAge() >= 40) {
		        // Add 20% per 5 years above 40
		        int age = user.getAge() - 40;
		        while (age >= 5) {
		            premium *= 1.2;
		            age -= 5;
		        }
		    }
		    // Add gender specific adjustments
		    if (user.getGender().equalsIgnoreCase("Male")) {
		        premium *= 1.02;
		    }
		    if(user.getCurrentHealth().size() > 0 ) {
		    	// Increase 1% per health issue
			    for (String currentHealth : user.getCurrentHealth()) {
			    	if(currentHealth.equalsIgnoreCase("Hypertension") 
			    			||currentHealth.equalsIgnoreCase("Blood pressure") ||
			    			currentHealth.equalsIgnoreCase("Blood sugar") ||
			    			currentHealth.equalsIgnoreCase("Overweight")) {
			    		premium *= 1.01;	
			    	}   
			    }	 
		    }
		    // Decrease 3% per good habbit
		    if(user.getHabits().size() > 0) {
		    	for (String habbit : user.getHabits()) {
			        if (habbit.equalsIgnoreCase("Daily exercise")) {
			            premium *= 0.97;
			        }else if(habbit.equalsIgnoreCase("alcohol") ||
			        		habbit.equalsIgnoreCase("Smoking")||
			        		habbit.equalsIgnoreCase("Drugs")) {
			        	premium *= 1.03;   	
			        }
			    }	
		    }
		    return premium;
		}
	}
